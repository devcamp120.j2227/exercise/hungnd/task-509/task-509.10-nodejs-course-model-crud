
// khai báo thư viện express
const express = require('express');

// Khai báo middleware
const {
    getAllReviewsMiddleware,
    getReviewsMiddleware,
    postReviewsMiddleware,
    putReviewsMiddleware,
    deleteReviewsMiddleware
} = require('../middleware/reviewMiddleware');

// Tạo ra Router
const reviewRouter = express.Router();
// dùng use sẽ truyền hết mỗi lần chạy
reviewRouter.use(getAllReviewsMiddleware);
// dùng cho từng phương thức
reviewRouter.get('/review', getAllReviewsMiddleware, (req, res) => {
    res.json({
        message: "Get all Reviews"
    })
});

reviewRouter.post('/review', postReviewsMiddleware, (req, res) => {
    res.json({
        message: "Create new Reviews"
    })
});

reviewRouter.put('/review/:reviewId', putReviewsMiddleware, (req, res) => {
    let reviewId = req.params.reviewId;
    res.json({
        message: `Update Reviews id = ${reviewId}`
    })
});

reviewRouter.delete('/review/:reviewsId', deleteReviewsMiddleware, (req, res) => {
    let reviewId = req.params.reviewId;
    res.json({
        message: `Delete Reviews id = ${reviewId}`
    })
});

reviewRouter.get('/review/:reviewsId', getReviewsMiddleware, (req, res) => {
    let reviewId = req.params.reviewId;
    res.json({
        message: `Get Reviews id = ${reviewId}`
    })
});
module.exports = { reviewRouter }